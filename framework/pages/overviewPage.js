const profileNameField = '.user > .dropdown > .dropdown-trigger > .button > .username'

const OverviewPage = {
    getProfileName: async(page) => {
        const profileNameText = await page.textContent(profileNameField)
        return profileNameText
    }
}

export default OverviewPage;
