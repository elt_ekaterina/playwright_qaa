import MainPage from "./mainPage";
import OverviewPage from "./overviewPage";

const app = () => ({
    Main: () => ({...MainPage}),
    Overview: () => ({...OverviewPage}),
})

export default app;
