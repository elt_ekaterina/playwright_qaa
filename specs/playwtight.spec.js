const assert = require('assert');
import { runBrowser, stopBrowser } from '../lib/browser';
import app from '../framework/pages/index'

describe ('UI Demo suit', () =>{
    let page;
    beforeEach( async () => {
        page = await runBrowser('https://try.vikunja.io/');

    })
    afterEach( async () => {
        await stopBrowser();
    })

    it('UI test', async () => {
        await app().Main().login(page, 'demo', 'demo');
        const profileNameText = await app().Overview().getProfileName(page)
        assert.deepEqual(profileNameText, 'demo');
    })
})
