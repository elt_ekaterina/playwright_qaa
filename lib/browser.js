import playwright from 'playwright';

let browser, context, page;

async function runBrowser(url) {
    browser = await playwright.chromium.launch({
        headless: false,
        slowMo: 250,
    });
    context = await browser.newContext();
    page = await context.newPage();
    await page.goto(url);
    return page;
}

async function stopBrowser() {
    await browser.close();
}

export {runBrowser, stopBrowser}
